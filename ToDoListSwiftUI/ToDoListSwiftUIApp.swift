//
//  ToDoListSwiftUIApp.swift
//  ToDoListSwiftUI
//
//  Created by Salvador Lopez on 13/06/23.
//

import SwiftUI

@main
struct ToDoListSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
