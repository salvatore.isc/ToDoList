//
//  ContentView.swift
//  ToDoListSwiftUI
//
//  Created by Salvador Lopez on 13/06/23.
//

import SwiftUI

//MARK: VIEW

struct TodoListView: View{
    
    @ObservedObject var viewModel: TodoViewModel
    
    var body: some View{
        List(viewModel.todos){ todo in
            HStack{
                Text(todo.titulo)
                Spacer()
                Image(systemName: todo.completed ? "checkmark.circle.fill" : "circle")
                    .foregroundColor(todo.completed ? .blue : .gray)
                    .onTapGesture {
                        viewModel.toggleCompleted(for: todo)
                    }
            }
        }
    }
}


//MARK: ContentView

struct ContentView: View {
    var body: some View {
        TodoListView(viewModel: TodoViewModel())
    }
}

//MARK: Preview

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
