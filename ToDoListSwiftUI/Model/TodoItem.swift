//
//  TodoItem.swift
//  ToDoListSwiftUI
//
//  Created by Salvador Lopez on 13/06/23.
//

import Foundation

//MARK: MODEL

struct TodoItem: Identifiable{
    var id = UUID()
    var titulo: String
    var completed: Bool
}
