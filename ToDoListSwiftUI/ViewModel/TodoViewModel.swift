//
//  TodoViewModel.swift
//  ToDoListSwiftUI
//
//  Created by Salvador Lopez on 13/06/23.
//

import Foundation

class TodoViewModel: ObservableObject{
    @Published var todos: [TodoItem] = [
        TodoItem(titulo: "Aprender Swift", completed: false),
        TodoItem(titulo: "Crear una App en iOS", completed: true),
        TodoItem(titulo: "Publicar en la tienda la App de iOS", completed: false)
    ]
    
    func toggleCompleted(for todo: TodoItem){
        if let index = todos.firstIndex(where: {
            $0.titulo == todo.titulo
        }){
            todos[index].completed.toggle()
        }
    }
}
